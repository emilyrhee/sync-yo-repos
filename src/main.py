#!/usr/bin/env python3

import os
import subprocess
import logging
import json
import sys

def config(
    path: str = f"{os.environ['XDG_CONFIG_HOME']}/syr/config.json"
) -> dict[str, list[dict]]:
    path = os.path.abspath(path)

    parent_dir = os.path.dirname(path)
    if not os.path.exists(parent_dir):
        os.mkdir(parent_dir)

    if os.path.exists(path):
        with open(path, "r") as file:
            return json.load(file)
    else:
        with open(path, "w") as file:
            config = { "repos": [{
                "link": "https://gitlab.com/cvoges12/sync-yo-repos",
                "path": os.getcwd()
            }]}
            file.write(
                json.dumps(config, indent = 2)
            )
            return config

def sync(config: dict[str, list[dict]]):
    for repo in config["repos"]:
        if os.path.exists(repo["path"]):
            if repo["branch"]:
                git_pull(repo["path"], repo["link"], repo["branch"])
            else:
                git_pull(repo["path"])
        else:
            git_clone(repo["link"], repo["path"])

def run(
    line: list[str],
    message: str,
    directory: str = None
):

    if directory:
        wd = os.getcwd()
        os.chdir(directory)
        p = subprocess.run(line, capture_output=True)
        os.chdir(wd)
    else:
        p = subprocess.run(line, capture_output=True)

    if p.stderr:
        logging.error(p.stderr.decode())
        if p.stdout != b"":
            logging.info(message)
    else:
        logging.info(message)

def git_clone(repo: str, path: str = None):
    if path:
        run(
            ["git", "clone", "-q", repo, path],
            f"Cloning {repo} into {path}"
        )
    else:
        run(
            ["git", "clone", "-q", repo],
            f"Cloning {repo}"
        )

def git_pull(path: str = None, repo: str = None, branches: list(str) = None):
    if branches and repo:
        for branch in branches:
            run(
                ["git", "pull", branch, "-q"],
                f"Pulling at {path}",
                path
            )
    else:
        run(
            ["git", "pull", "-q"],
            f"Pulling at {path}",
            path
        )

if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="[%(levelname)s] %(message)s"
    )
    logging.getLogger(__name__)

    if sys.argv == ['']:
        sync(config())
    else:
        sync(config(sys.argv[1]))
